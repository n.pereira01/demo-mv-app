package com.demo.mv.app.controllers;

import com.demo.mv.app.data.UfData;
import com.demo.mv.app.services.UfService;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("/api")
public class DataController {

    @GetMapping("/data")
    public ResponseEntity<?> getData() {
        UfService ufService = new UfService();
        Map<String, Object> data = new HashMap<String, Object>();

        try {
            data = ufService.getHtml();
        }catch (IOException e) {
            System.out.println(e.getMessage());
        }

        return new ResponseEntity<>(data, HttpStatus.OK);
    }


}
