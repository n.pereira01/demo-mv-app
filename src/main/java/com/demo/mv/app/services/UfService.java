package com.demo.mv.app.services;

import com.demo.mv.app.data.UfData;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
public class UfService {

    private static final String URL = "https://si3.bcentral.cl/indicadoressiete/secure/Serie.aspx?gcode=UF&param=RABmAFYAWQB3AGYAaQBuAEkALQAzADUAbgBNAGgAaAAkADUAVwBQAC4AbQBYADAARwBOAGUAYwBjACMAQQBaAHAARgBhAGcAUABTAGUAYwBsAEMAMQA0AE0AawBLAF8AdQBDACQASABzAG0AXwA2AHQAawBvAFcAZwBKAEwAegBzAF8AbgBMAHIAYgBDAC4ARQA3AFUAVwB4AFIAWQBhAEEAOABkAHkAZwAxAEEARAA=";

    public Map<String, Object> getHtml() throws IOException {
        Document doc = Jsoup.connect(URL).get();
        List<String> values = new ArrayList<>();
        //TABLA CON ID GR
        Element table = doc.select("table[id=gr]").first();
        //ELEMENTOS DE LA TABLA
        Elements elements = doc.select("table[id=gr]");

        //header de la tabla
        //Elements header = elements.select("tr[class=GridHeader]");
        Elements tr = elements.select("tr");
        Elements td = tr.select("td");
        /*
        Elements rows = elements.get(0).select("tr");
        */

        Map<String, Object> data = new HashMap<>();
        String valor = "";
        int i;
        for(i = 1; i<tr.size(); i++) {
            for(var j=1; j<tr.get(i).select("td").size(); j++) {
                valor = tr.get(i).select("td").get(j).text();
                values.add(valor);
            }
        }

        data.put("1", values);

        return data;
    }
}
