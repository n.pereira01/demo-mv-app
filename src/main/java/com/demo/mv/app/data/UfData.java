package com.demo.mv.app.data;

public class UfData {
    private String valEnero;
    private String valFebrero;
    private String valMarzo;
    private String valAbril;
    private String valMayo;
    private String valJunio;
    private String valJulio;
    private String valAgosto;
    private String valSeptiembre;
    private String valOctubre;
    private String valNoviembre;
    private String valDiciembre;

    public UfData() {}

    public UfData(
            String valEnero,
            String valFebrero,
            String valMarzo,
            String valAbril,
            String valMayo,
            String valJunio,
            String valJulio,
            String valAgosto,
            String valSeptiembre,
            String valOctubre,
            String valNoviembre,
            String valDiciembre
    ) {
        this.valEnero = valEnero;
        this.valFebrero = valFebrero;
        this.valMarzo = valMarzo;
        this.valAbril = valAbril;
        this.valMayo = valMayo;
        this.valJunio = valJunio;
        this.valJulio = valJulio;
        this.valAgosto = valAgosto;
        this.valSeptiembre = valSeptiembre;
        this.valOctubre = valOctubre;
        this.valNoviembre = valNoviembre;
        this.valDiciembre = valDiciembre;
    }

    public void setValEnero(String valEnero) {
        this.valEnero = valEnero;
    }

    public void setValFebrero(String valFebrero) {
        this.valFebrero = valFebrero;
    }

    public void setValMarzo(String valMarzo) {
        this.valMarzo = valMarzo;
    }

    public void setValAbril(String valAbril) {
        this.valAbril = valAbril;
    }

    public void setValMayo(String valMayo) {
        this.valMayo = valMayo;
    }

    public void setValJunio(String valJunio) {
        this.valJunio = valJunio;
    }

    public void setValJulio(String valJulio) {
        this.valJulio = valJulio;
    }

    public void setValAgosto(String valAgosto) {
        this.valAgosto = valAgosto;
    }

    public void setValSeptiembre(String valSeptiembre) {
        this.valSeptiembre = valSeptiembre;
    }

    public void setValOctubre(String valOctubre) {
        this.valOctubre = valOctubre;
    }

    public void setValNoviembre(String valNoviembre) {
        this.valNoviembre = valNoviembre;
    }

    public void setValDiciembre(String valDiciembre) {
        this.valDiciembre = valDiciembre;
    }
}
